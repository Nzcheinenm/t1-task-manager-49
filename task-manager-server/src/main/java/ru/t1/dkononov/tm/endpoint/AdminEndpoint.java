package ru.t1.dkononov.tm.endpoint;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkononov.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dkononov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkononov.tm.api.services.IAdminService;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.api.services.IServiceLocator;
import ru.t1.dkononov.tm.api.services.dto.ISessionDTOService;
import ru.t1.dkononov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.dkononov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.dkononov.tm.dto.request.DropSchemeRequest;
import ru.t1.dkononov.tm.dto.request.InitSchemeRequest;
import ru.t1.dkononov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.dkononov.tm.dto.response.ApplicationVersionResponse;
import ru.t1.dkononov.tm.dto.response.DropSchemeResponse;
import ru.t1.dkononov.tm.dto.response.InitSchemeResponse;
import ru.t1.dkononov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.IAdminEndpoint")
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private IAdminService getAdminService() {
        return serviceLocator.getAdminService();
    }


    @NotNull
    @Override
    @WebMethod
    public DropSchemeResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final DropSchemeRequest request

    ) {
        getAdminService().dropScheme(request.getToken());
        return new DropSchemeResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public InitSchemeResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final InitSchemeRequest request

    ) {
        getAdminService().initScheme(request.getToken());
        return new InitSchemeResponse();
    }

}

