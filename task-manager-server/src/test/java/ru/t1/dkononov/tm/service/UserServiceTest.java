package ru.t1.dkononov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.dto.model.UserDTO;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.exception.AbstractException;
import ru.t1.dkononov.tm.exception.field.AbstractFieldException;
import ru.t1.dkononov.tm.exception.field.LoginEmptyException;
import ru.t1.dkononov.tm.exception.field.UserNotFoundException;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.model.User;
import ru.t1.dkononov.tm.service.dto.UserDTOService;
import ru.t1.dkononov.tm.service.model.UserService;

import java.util.Objects;

import static ru.t1.dkononov.tm.constant.TestData.LOGIN;
import static ru.t1.dkononov.tm.constant.TestData.PASSWORD;


@Category(UnitCategory.class)
public class UserServiceTest extends AbstractSchemaTest {


    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final UserDTOService service = new UserDTOService(propertyService,connectionService);

    @NotNull
    private static final String LOGIN_TEST = "logintest";

    @NotNull
    private static final String PASS_TEST = "logintest";

    @NotNull
    private static final String PASS_RETEST = "logintest";

    @NotNull
    private static final String NAME = "firstName";

    private @NotNull UserDTO userTesting;

    @Before
    public void before() throws AbstractException, LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        userTesting = service.create(LOGIN_TEST, PASS_TEST, Role.USUAL);
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void create() throws AbstractException {
        service.create(LOGIN, PASSWORD, Role.USUAL);
        @Nullable final UserDTO user = service.findByLogin(LOGIN);
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLogin() throws LoginEmptyException, UserNotFoundException {
        Assert.assertNotNull(service.findByLogin(LOGIN_TEST));
    }


    @Test
    public void removeByLogin() throws AbstractFieldException {
        service.removeByLogin(LOGIN_TEST);
        Assert.assertThrows(Exception.class, () -> service.findByLogin(LOGIN_TEST));
    }

    @Test
    public void updateUser() throws AbstractFieldException {
        service.updateUser(userTesting.getId(), NAME, "lastName", "middle");
        Assert.assertTrue(Objects.equals(userTesting.getFirstName(), NAME));

    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(service.isLoginExist(LOGIN_TEST));
    }


}
